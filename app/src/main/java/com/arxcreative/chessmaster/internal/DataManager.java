package com.arxcreative.chessmaster.internal;

import com.arxcreative.chessmaster.model.UserModel;

import org.androidannotations.annotations.EBean;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 12:00 PM
 */
@EBean(scope = EBean.Scope.Singleton)
public class DataManager {

    @Setter
    @Getter
    UserModel user;
}
