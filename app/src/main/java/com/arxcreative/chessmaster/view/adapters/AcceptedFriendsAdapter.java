package com.arxcreative.chessmaster.view.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.arxcreative.chessmaster.controllers.FriendsProvider;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.view.component.AcceptedFriendsView;
import com.arxcreative.chessmaster.view.component.AcceptedFriendsView_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EBean
public class AcceptedFriendsAdapter extends RecyclerViewAdapterBase<UserModel, AcceptedFriendsView>{

    @Bean
    FriendsProvider provider;

    @RootContext
    Context context;

    @Override
    protected AcceptedFriendsView onCreateItemView(ViewGroup parent, int viewType){
        return AcceptedFriendsView_.build(context);
    }

    @Override
    public UserModel getObject(int position) { return provider.getAcceptedFriend(position); }

    @Override
    public void onBindViewHolder(ViewWrapper<AcceptedFriendsView> holder, int position) {
        holder.getView().bind(getObject(position));
    }

    @Override
    public int getItemCount() {
        return provider.getAcceptedFriendsCount();
    }
}
