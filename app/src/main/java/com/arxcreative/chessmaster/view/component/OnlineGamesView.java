package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.model.GameModel;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import carbon.widget.LinearLayout;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 1:47 PM
 */
@EViewGroup(R.layout.item_online_recent)
public class OnlineGamesView extends LinearLayout {

    @ViewById(R.id.playerOneName)
    TextView playerOneName;
    @ViewById(R.id.playerTwoName)
    TextView playerTwoName;
    @ViewById(R.id.winner)
    TextView winner;
    @ViewById(R.id.points)
    TextView points;

    public OnlineGamesView(Context context) {
        super(context);
    }

    public OnlineGamesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OnlineGamesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(GameModel model) {
        playerOneName.setText(model.getPlayer_one().getName());
        playerTwoName.setText(model.getPlayer_two().getName());
        points.setText("Points: "+model.getPoints());
        String winnerName = null;
        if(model.getPlayer_one().getId()==model.getWinner()){
            winnerName = model.getPlayer_one().getName();
        } else {
            winnerName = model.getPlayer_two().getName();
        }
        winner.setText("Winner: "+winnerName);
    }
}
