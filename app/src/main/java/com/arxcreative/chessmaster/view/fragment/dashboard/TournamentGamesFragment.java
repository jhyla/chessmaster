package com.arxcreative.chessmaster.view.fragment.dashboard;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.TournamentsProvider;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.adapters.LatestTournamentsAdapter;
import com.arxcreative.chessmaster.view.adapters.TournamentsAdapter;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import carbon.widget.RecyclerView;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 10:58 AM
 */
@EFragment(R.layout.fragment_tournaments)
public class TournamentGamesFragment extends Fragment {

    @ViewById(R.id.tournaments)
    RecyclerView tournaments;

    @ViewById(R.id.latestTournaments)
    RecyclerView latestTournaments;

    @Bean
    LatestTournamentsAdapter latestAdapter;

    @Bean
    TournamentsAdapter tournamentsAdapter;

    @Bean
    TournamentsProvider tournamentsProvider;

    @Bean
    EventBus eventBus;

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();

    }

    @Override
    public void onResume() {
        eventBus.register(this);
        if (tournamentsProvider.areLatestTournamentsFetched()) {
            latestAdapter.notifyDataSetChanged();
        } else {
            tournamentsProvider.fetchLatestTournaments();
        }

        if (tournamentsProvider.areTournamentsFetched()) {
            tournamentsAdapter.notifyDataSetChanged();
        } else {
            tournamentsProvider.fetchTournaments();
        }
        super.onResume();
    }

    @AfterViews
    public void setViews(){
        tournaments.setAdapter(tournamentsAdapter);
        GridLayoutManager managerTournaments = new GridLayoutManager(
                getActivity(),
                1);

        tournaments.setLayoutManager(managerTournaments);

        latestTournaments.setAdapter(latestAdapter);
        GridLayoutManager latestTournamentsManager = new GridLayoutManager(
                getActivity(),
                1);

        latestTournaments.setLayoutManager(latestTournamentsManager);
    }

    @Subscribe
    public void onUpdate(TournamentsProvider.Update update){
        if (tournamentsProvider.areLatestTournamentsFetched()) {
            latestAdapter.notifyDataSetChanged();
        }

        if (tournamentsProvider.areTournamentsFetched()) {
            tournamentsAdapter.notifyDataSetChanged();
        }
    }
}
