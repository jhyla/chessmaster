package com.arxcreative.chessmaster.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 8/17/2015 10:42 AM
 */
public class DateUtils {
    public final static int API_DATE_FORMAT = 0;
    public final static int DAY_DATE = 1;
    public final static int LESSON_DISPLAY_FORMAT = 2;
    public final static int HOUR_DATE = 3;

    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static SimpleDateFormat[] dateFormats = {
            new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US),
            new SimpleDateFormat("yyyy-MM-dd",Locale.US),
            new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.US),
            new SimpleDateFormat("HH:mm:ss", Locale.US)
    };

    public static SimpleDateFormat getDateFormat(int format) {
        if(format<0||format>=dateFormats.length)
            throw new IllegalArgumentException("Unknown SimpleDateFormat format");

        return dateFormats[format];
    }
}
