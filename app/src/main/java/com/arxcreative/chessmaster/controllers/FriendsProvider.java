package com.arxcreative.chessmaster.controllers;

import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.GameModel;
import com.arxcreative.chessmaster.model.GetFriendsResponse;
import com.arxcreative.chessmaster.model.StatusResponse;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.networking.RestClient;
import com.arxcreative.chessmaster.utils.EventBus;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EBean(scope = EBean.Scope.Singleton)
public class FriendsProvider {

    public enum State{
        Fetched, Fetching, Error
    }

    @Bean
    DataManager dataManager;

    @Bean
    EventBus eventBus;

    private State friendsState = null;
    private State state = null;

    private final ArrayList<UserModel> acceptedFriends = new ArrayList<>();
    private final ArrayList<UserModel> pendingFriends = new ArrayList<>();
    private final ArrayList<UserModel> sentFriends = new ArrayList<>();

    private String friendsError;
    private String acceptRejectError;

    @AfterInject
    public void registerBus(){eventBus.register(this);}

    @Background
    public void fetchFriends(){
        GetFriendsResponse response = null;

        try {
            response = RestClient.getInstance().getFriends(dataManager.getUser().getId());
        } catch(TimeoutException exception){
            onFetchFriendsUnknownError();
            return;
        }

        if (response.getStatus()){
            onFetchFriendSuccess(response.getAccepted(), response.getPending(), response.getSent());
        } else {
            onFetchFriendsFailure();
        }
    }

    @UiThread
    public void onFetchFriendsUnknownError(){
        this.friendsError = "Unknown error";
        setFetchFriendsState(State.Error);
        return;
    }

    @UiThread
    public void onFetchFriendSuccess(ArrayList<UserModel> acceptedFriends, ArrayList<UserModel> pendingFriends, ArrayList<UserModel> sentFriends){
        this.acceptedFriends.clear();
        this.pendingFriends.clear();
        this.sentFriends.clear();

        this.acceptedFriends.addAll(acceptedFriends);
        this.pendingFriends.addAll(pendingFriends);
        this.sentFriends.addAll(sentFriends);
        setFetchFriendsState(State.Fetched);
    }

    public void onFetchFriendsFailure(){
        this.friendsError = "Unknown error";
        setFetchFriendsState(State.Error);
    }

    public void setFetchFriendsState(State state){
        this.friendsState = state;
        eventBus.post(new Update());
    }

    public boolean areFriendsFetched() {return friendsState == State.Fetched; }

    public boolean hasFriendsError() { return friendsState == State.Error; }

    public String getFriendsError(){ return friendsError; }

    public int getAcceptedFriendsCount() {return acceptedFriends.size(); }

    public int getPendingFriendsCount() {return pendingFriends.size(); }

    public int getSentFriendsCount() {return sentFriends.size(); }

    public UserModel getAcceptedFriend(int position){ return acceptedFriends.get(position); }

    public UserModel getPendingFriend(int position){ return pendingFriends.get(position); }

    public UserModel getSentFriend(int position){ return sentFriends.get(position); }

    @Background
    public void acceptFriend(int friendId){
        StatusResponse response;

        try {
            response = RestClient.getInstance().acceptFriendRequest(dataManager.getUser().getId(), friendId);
        } catch(TimeoutException exception){
            onFetchFriendsUnknownError();
            return;
        }

        if(response.getStatus()){
            onAcceptSuccess();
        } else {
            onAcceptRejectFailure(response.getError());
        }
    }

    @Background
    public void rejectFriend(int friendId){
        StatusResponse response;

        try {
            response = RestClient.getInstance().declineFriendRequest(dataManager.getUser().getId(), friendId);
        } catch(TimeoutException exception){
            onFetchFriendsUnknownError();
            return;
        }

        if(response.getStatus()){
            onRejectSuccess();
        } else {
            onAcceptRejectFailure(response.getError());
        }
    }

    @UiThread
    public void onAcceptRejectFailure(String error) {
        this.acceptRejectError = error;
        setAcceptRejectState(State.Error);
    }

    private void setAcceptRejectState(State state) {
        this.state = state;
        eventBus.post(new Update());
    }

    @UiThread
    public void onAcceptSuccess() {
        setAcceptRejectState(State.Fetched);
    }

    @UiThread
    public void onRejectSuccess(){
        setAcceptRejectState(State.Fetched);
    }

    public class Update {

    }
}
