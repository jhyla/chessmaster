package com.arxcreative.chessmaster.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 12:07 PM
 */
@Getter
@Setter
public class SearchForFriendResponse {
    ArrayList<UserModel> users;
    Boolean status;
    String error;
}
