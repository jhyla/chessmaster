package com.arxcreative.chessmaster.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 12:06 PM
 */
@Getter
@Setter
public class GetUserResponse {
    UserModel user;
    Boolean status;
    String error;
}
