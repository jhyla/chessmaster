package com.arxcreative.chessmaster.controllers;

import android.util.Base64;
import android.util.Log;

import com.arxcreative.chessmaster.BuildConfig;
import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.SignInRequest;
import com.arxcreative.chessmaster.model.SignInResponse;
import com.arxcreative.chessmaster.model.SignUpRequest;
import com.arxcreative.chessmaster.model.SignUpResponse;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.networking.RestClient;
import com.arxcreative.chessmaster.utils.EventBus;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import lombok.Getter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 3:51 PM
 */
@EBean(scope = EBean.Scope.Singleton)
public class ConnectionController {

    private State registrationState;
    private State loginState;
    private String registrationError;
    private String loginError;

    @Bean
    DataManager dataManager;

    @Bean
    EventBus eventBus;

    @Getter
    private UserModel userData;

    public enum State {
        SUCCESS,
        FAILURE
    }

    public enum Action {
        REGISTER,
        LOGIN
    }

    @AfterInject
    public void registerBus() {
        eventBus.register(this);
    }

    @Background
    public void register(String login, String password, String name) {
        registrationError=null;
        registrationState=null;
        SignUpResponse response = null;
        try {
            byte[] passwordBytes = password.getBytes("UTF-8");
            String passwordEncoded = Base64.encodeToString(passwordBytes, Base64.DEFAULT);

            response = RestClient.getInstance().signUp(login,passwordEncoded,name,null
            );
        } catch (TimeoutException | UnsupportedEncodingException e) {
            onUnexpectedError(Action.REGISTER);
            return;
        }

        if (response.getStatus() == null) {
            onUnexpectedError(Action.REGISTER);
            return;
        }
        if (response.getStatus()) {
            onRegistrationSuccess(response);
        } else {
            onRegistrationFailure(response);
        }
    }

    @Background
    public void login(String login, String password) {
        SignInResponse response = null;
        try{
            byte[] passwordBytes = password.getBytes("UTF-8");
            String passwordEncoded = Base64.encodeToString(passwordBytes, Base64.DEFAULT);
            log(passwordEncoded);
            response = RestClient.getInstance().login(login,passwordEncoded);/*SignInRequest.builder()
                    .login(login)
                    .password(passwordEncoded).build());*/
        } catch (TimeoutException|UnsupportedEncodingException exception){
            log("Unknown");
            onLoginUnknownError();
            return;
        }

        if(response.getStatus()){
            log("Success");
            onLoginSuccess(response.getUser());
        } else {
            log("Failure");
            onLoginFailure(response.getError());
        }
    }

    private void log(String s) {
        if(BuildConfig.DEBUG){
            Log.i("--CONN--",s);
        }
    }

    @UiThread
    public void onLoginFailure(String error) {
        setError(Action.LOGIN,error);
        changeState(Action.LOGIN, State.FAILURE);
    }

    @UiThread
    public void onLoginSuccess(UserModel user) {
        dataManager.setUser(user);
        changeState(Action.LOGIN,State.SUCCESS);
    }

    @UiThread
    public void onLoginUnknownError() {
        setError(Action.LOGIN,"Unknown error");
        changeState(Action.LOGIN,State.FAILURE);
    }

    @UiThread
    public void onRegistrationFailure(SignUpResponse response) {
        setError(Action.REGISTER, response.getError());
        changeState(Action.REGISTER, State.FAILURE);
    }

    private void changeState(Action action, State state) {
        switch (action) {
            case REGISTER:
                registrationState = state;
                break;
            case LOGIN:
                loginState = state;
                break;
        }
        eventBus.post(new Update());
    }

    public State getState(Action action) {
        switch (action) {
            case REGISTER:
                return registrationState;
            case LOGIN:
                return loginState;
        }
        return null;
    }

    public String getError(Action action) {
        switch (action) {
            case REGISTER:
                return registrationError;
            case LOGIN:
                return loginError;
        }
        return null;
    }

    public void setError(Action action, String error) {
        switch (action) {
            case REGISTER:
                registrationError = error;
                break;
            case LOGIN:
                loginError = error;
                break;
        }
    }

    @UiThread
    public void onRegistrationSuccess(SignUpResponse response) {
       dataManager.setUser(response.getUser());
        changeState(Action.REGISTER, State.SUCCESS);
    }

    @UiThread
    public void onUnexpectedError(Action action) {
        setError(action, "Unkown error");
        changeState(action, State.FAILURE);
    }

    public class Update {
    }
}
