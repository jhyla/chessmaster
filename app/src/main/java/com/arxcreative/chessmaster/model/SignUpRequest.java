package com.arxcreative.chessmaster.model;

import lombok.Getter;
import lombok.experimental.Builder;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/22/2015 11:23 PM
 */
@Getter
@Builder
public class SignUpRequest {
    String name;
    String login;
    String password;
    String avatar;
}
