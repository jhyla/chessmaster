package com.arxcreative.chessmaster.model;

import java.util.ArrayList;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 12:02 PM
 */
@Getter
@Setter
public class TournamentModel {
    Integer id;
    String name;
    Integer max_users;
    Integer min_points;
    Integer max_points;
    Date date_created;
    Date due_date;
    Integer type;
    ArrayList<UserModel> players;
    Integer best_player;
    ArrayList<RankingModel> ranking;
}
