package com.arxcreative.chessmaster.controllers;

import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.SearchForFriendResponse;
import com.arxcreative.chessmaster.model.StatusResponse;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.networking.RestClient;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.utils.ToastUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EBean(scope = EBean.Scope.Singleton)
public class NewFriendProvider {

    public enum State{
        Fetched, Fetching, Error
    }

    @Bean
    DataManager dataManager;

    @Bean
    EventBus eventBus;

    private State usersState = null;
    private State state = null;


    private final ArrayList<UserModel> users = new ArrayList<>();

    private String usersError;
    private String error;

    @AfterInject
    public void registerBus(){eventBus.register(this);}

    @Background
    public void findFriends(String name){
        SearchForFriendResponse response = null;

        try {
            response = RestClient.getInstance().searchForFriend(dataManager.getUser().getId(), name);
        } catch(TimeoutException exception){
            onFetchFriendsUnknownError();
            return;
        }

        if (response.getStatus()){
            onFetchFriendSuccess(response.getUsers());
        } else {
            onFetchFriendsFailure();
        }
    }

    @UiThread
    public void onFetchFriendsUnknownError(){
        this.usersError = "Unknown error";
        setFetchFriendsState(State.Error);
        return;
    }

    @UiThread
    public void onFetchFriendSuccess(ArrayList<UserModel> users){
        this.users.clear();

        this.users.addAll(users);
        setFetchFriendsState(State.Fetched);
    }

    public void onFetchFriendsFailure(){
        this.usersError = "Unknown error";
        setFetchFriendsState(State.Error);
    }

    public void setFetchFriendsState(State state){
        this.usersState = state;
        eventBus.post(new Update());
    }

    public boolean areFriendsFetched() {return usersState == State.Fetched; }

    public boolean hasFriendsError() { return usersState == State.Error; }

    public String getFriendsError(){ return usersError; }

    public int getNewFriendsCount() {return users.size(); }

    public UserModel getFriend(int position){ return users.get(position); }

    @Background
    public void newFriend(int id){
        StatusResponse response;

        try {
            response = RestClient.getInstance().newFriend(dataManager.getUser().getId(), id);
        } catch(TimeoutException exception){
            onFetchFriendsUnknownError();
            return;
        }

        if(response.getStatus()){
            onSuccess();
        } else {
            onFailure(response.getError());
        }
    }

    @UiThread
    public void onFailure(String error) {
        this.error = error;
        setState(State.Error);
    }

    private void setState(State state) {
        this.state = state;
        eventBus.post(new Update());
    }

    @UiThread
    public void onSuccess() {
        setState(State.Fetched);
//        ToastUtils toast = new ToastUtils();
//        toast.displayShortToast("Friend request sent.");
        System.out.println("Friend request sent");
    }

    public class Update {

    }
}
