package com.arxcreative.chessmaster.view.fragment;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.ConnectionController;
import com.arxcreative.chessmaster.utils.EventBus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;



/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 8:12 PM
 */
@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment {

    @Bean
    AppStateController appStateController;

    @ViewById(R.id.passwordEditText)
    EditText password;

    @ViewById(R.id.loginEditText)
    EditText login;

    @Bean
    ConnectionController controller;

    @Bean
    EventBus eventBus;

    @Override
    public void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
        if(controller.getState(ConnectionController.Action.LOGIN)!=null) {
            if (controller.getState(ConnectionController.Action.LOGIN) == ConnectionController.State.SUCCESS) {
                appStateController.openDashboard();
            } else {
                Snackbar snackbar = Snackbar.make(this.getView(), controller.getError(ConnectionController.Action.LOGIN), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Click(R.id.loginButton)
    public void onLoginButtonClicked(){
        if(!formValidation()){
            showNonValidFormInformation();
            return;
        }

        controller.login(login.getText().toString(),password.getText().toString());
    }

    private void showNonValidFormInformation() {
        Snackbar snackbar = Snackbar.make(this.getView(),"Form not valid!",Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private boolean formValidation() {
        return !password.getText().toString().isEmpty() && !login.getText().toString().isEmpty();
    }

    @Subscribe
    public void onUpdate(ConnectionController.Update update){
        if(controller.getState(ConnectionController.Action.LOGIN)== ConnectionController.State.SUCCESS){
            appStateController.openDashboard();
        } else {
            Snackbar snackbar = Snackbar.make(this.getView(),controller.getError(ConnectionController.Action.LOGIN),Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Click(R.id.createNewAccountButton)
    public void onCreateNewAccountClicked(){
        appStateController.openRegister();
    }
}
