package com.arxcreative.chessmaster.controllers;

import android.util.Log;

import com.arxcreative.chessmaster.BuildConfig;
import com.arxcreative.chessmaster.utils.EventBus;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 4:08 PM
 */

@EBean(scope= EBean.Scope.Singleton)
public class AppStateController {

    @Bean
    ConnectionController connectionController;

    @Bean
    EventBus eventBus;

    @Getter
    private State currentState = State.LOGIN;

    @Setter
    @Getter
    private String gameLevelString;

    public void openLogin() {
        setCurrentState(State.LOGIN);
    }

    public void openRegister(){
        setCurrentState(State.REGISTER);
    }

    public void openDashboard() {
        setCurrentState(State.DASHBOARD);
    }

    public void openNewFriend() { setCurrentState(State.NEW_FRIEND); }

    private void log(String s) {
        if(BuildConfig.DEBUG){
            Log.i("--APPSTATE--", s);
        }
    }

    public boolean isPlayingOffline() {
        return true;
    }

    public void startOfflineGame(String item) {
        setGameLevelString(item);
        setCurrentState(State.OFFLINE);
    }

    public enum State{
        LOGIN, DASHBOARD, OFFLINE, REGISTER, NEW_FRIEND
    }

    private void setCurrentState(State currentState){
        this.currentState = currentState;
        notifyChanged();
    }

    public State getCurrentState(){
        return currentState;
    }

    private void notifyChanged() {
        eventBus.post(new Update());
    }

    public class Update {
    }
}
