package com.arxcreative.chessmaster.view.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import com.arxcreative.chessmaster.BuildConfig;
import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.ConnectionController;
import com.arxcreative.chessmaster.utils.EventBus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;


/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 4:46 PM
 */
@EFragment(R.layout.fragment_registration)
public class RegisterFragment extends Fragment{

    @Bean
    AppStateController appStateController;

    @Bean
    ConnectionController connectionController;

    @ViewById(R.id.nameEditText)
    EditText name;

    @ViewById(R.id.passwordEditText)
    EditText password;

    @ViewById(R.id.loginEditText)
    EditText login;

    @Bean
    EventBus eventBus;

    @Click(R.id.loginButton)
    public void onLoginButtonClicked(){
        appStateController.openLogin();
    }

    @Click(R.id.createNewAccountButton)
    public void onCreateNewAccountClicked(){
        if(!formValidation()){
            showNonValidFormInformation();
            return;
        }
        onRegister();
    }

    @Override
    public void onResume() {
        eventBus.register(this);
        update();
        super.onResume();
    }

    @Subscribe
    public void onConnectionUpdate(ConnectionController.Update update){
        update();
    }

    @Subscribe
    public void onUpdate(AppStateController.Update update){

    }

    private void update() {
        if(connectionController.getState(ConnectionController.Action.REGISTER)==null)
            return;
        if(connectionController.getState(ConnectionController.Action.REGISTER)== ConnectionController.State.FAILURE){
            String error = connectionController.getError(ConnectionController.Action.REGISTER);
            Snackbar.make(this.getView(),error, Snackbar.LENGTH_LONG).show();
        } else {
            appStateController.openDashboard();
        }
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    @Background
    public void onRegister() {
//        URL url = null;
//        try {
//            url = new URL("http://lorempixel.com/200/200/");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        log("here");
//        Bitmap image = null;
//        try {
//            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        log("here2");

//        String imageEncoded = encodeTobase64(image);
//        log(imageEncoded);
        connectionController.register(login.getText().toString(),
                password.getText().toString(),
                name.getText().toString());
    }

    private void log(String s) {
        if(BuildConfig.DEBUG){
            Log.i("--REGFRAG--", s);
        }
    }

    public static String encodeTobase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageEncoded;
    }
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }


    private void showNonValidFormInformation() {
        Snackbar snackbar = Snackbar.make(this.getView(),"Form not valid!",Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private boolean formValidation() {
        return !password.getText().toString().isEmpty() && !login.getText().toString().isEmpty() && !name.getText().toString().isEmpty();
    }
}
