package com.arxcreative.chessmaster.controllers;

import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.GameModel;
import com.arxcreative.chessmaster.model.GetLatestOfflineGamesResponse;
import com.arxcreative.chessmaster.model.GetLatestOnlineGamesResponse;
import com.arxcreative.chessmaster.networking.RestClient;
import com.arxcreative.chessmaster.utils.EventBus;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 4:10 PM
 */
@EBean(scope = EBean.Scope.Singleton)
public class OfflineGamesProvider {

    @Bean
    EventBus eventBus;

    @Bean
    DataManager dataManager;

    public int getCount() {
        return games.size();
    }

    public GameModel getGame(int position) {
        return games.get(position);
    }

    public enum State{
        Fetched, Fetching, Error
    }

    private State state = null;
    private String error = null;
    private final ArrayList<GameModel> games = new ArrayList<>();

    @AfterInject
    public void registerBus(){
        eventBus.register(this);
    }

    @Background
    public void fetchOnlineGames(){
        GetLatestOfflineGamesResponse response = null;
        try{
            response = RestClient.getInstance().getLatestOfflineGames(dataManager.getUser().getId());
        } catch (TimeoutException exception){
            onUnkownException();
        }

        if(response.getStatus()){
            onSuccess(response.getGames());
        } else {
            onFailure(response.getError());
        }
    }

    @UiThread
    public void onFailure(String error) {
        this.error = error;
        setState(State.Error);
    }

    private void setState(State state) {
        this.state = state;
        eventBus.post(new Update());
    }

    @UiThread
    public void onSuccess(ArrayList<GameModel> games) {
        if(games==null)
            games = new ArrayList<>();
        this.games.clear();
        this.games.addAll(games);
        setState(State.Fetched);
    }

    @UiThread
    public void onUnkownException() {
        this.error = "Unknown Error";
        setState(State.Error);
    }

    public boolean isFetched(){
        return state==State.Fetched;
    }

    public boolean hasError(){
        return state==State.Error;
    }

    public String getError(){
        return error;
    }

    public class Update {
    }
}
