package com.arxcreative.chessmaster.utils;

import android.content.Context;
import android.widget.Toast;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 3:53 PM
 */
@EBean(scope = EBean.Scope.Singleton)
public class ToastUtils {
    @RootContext
    Context context;

    public void displayShortToast(final String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public void displayLongToast(final String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
}
