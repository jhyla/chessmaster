package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.FriendsProvider;
import com.arxcreative.chessmaster.model.UserModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EViewGroup(R.layout.item_friend)
public class PendingFriendsView extends LinearLayout {

    @Bean
    FriendsProvider provider;

    @ViewById(R.id.avatar)
    ImageView avatar;

    @ViewById(R.id.name)
    TextView name;

    @ViewById(R.id.acceptRejectContainer)
    LinearLayout acceptRejectContainer;

    @ViewById(R.id.acceptButton)
    LinearLayout acceptButton;

    @ViewById(R.id.rejectButton)
    LinearLayout rejectButton;

    public PendingFriendsView(Context context) { super(context); }

    public PendingFriendsView(Context context, AttributeSet attrs)  {
        super(context, attrs);
    }

    public PendingFriendsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(final UserModel model){
        name.setText(model.getName());
        ImageLoader.getInstance().displayImage("http://" + model.getAvatar(), avatar);
        acceptRejectContainer.setVisibility(VISIBLE);

        acceptButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                provider.acceptFriend(model.getId());
                provider.fetchFriends();
            }
        });

        rejectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                provider.rejectFriend(model.getId());
                provider.fetchFriends();
            }
        });
    }
}
