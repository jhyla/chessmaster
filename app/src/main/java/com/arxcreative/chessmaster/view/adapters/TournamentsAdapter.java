package com.arxcreative.chessmaster.view.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.arxcreative.chessmaster.controllers.TournamentsProvider;
import com.arxcreative.chessmaster.model.TournamentModel;
import com.arxcreative.chessmaster.view.component.TournamentView;
import com.arxcreative.chessmaster.view.component.TournamentView_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 11:35 AM
 */
@EBean
public class TournamentsAdapter extends RecyclerViewAdapterBase<TournamentModel, TournamentView>{

    @Bean
    TournamentsProvider provider;

    @RootContext
    Context context;

    @Override
    protected TournamentView onCreateItemView(ViewGroup parent, int viewType) {
        return TournamentView_.build(context);
    }

    @Override
    public TournamentModel getObject(int position) {
        return provider.getTournament(position);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<TournamentView> holder, int position) {
        holder.getView().bind(getObject(position));
    }

    @Override
    public int getItemCount() {
        return provider.getTournamentsCount();
    }
}
