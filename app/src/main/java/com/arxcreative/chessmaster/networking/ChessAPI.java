package com.arxcreative.chessmaster.networking;

import com.arxcreative.chessmaster.model.GetFriendsResponse;
import com.arxcreative.chessmaster.model.GetLatestOfflineGamesResponse;
import com.arxcreative.chessmaster.model.GetLatestOnlineGamesResponse;
import com.arxcreative.chessmaster.model.GetLatestTournamentsResponse;
import com.arxcreative.chessmaster.model.GetTournamentGamesResponse;
import com.arxcreative.chessmaster.model.GetTournamentsResponse;
import com.arxcreative.chessmaster.model.GetUserResponse;
import com.arxcreative.chessmaster.model.SearchForFriendResponse;
import com.arxcreative.chessmaster.model.SearchForRandomOpponentResponse;
import com.arxcreative.chessmaster.model.SignInRequest;
import com.arxcreative.chessmaster.model.SignInResponse;
import com.arxcreative.chessmaster.model.SignUpResponse;
import com.arxcreative.chessmaster.model.StatusResponse;

import java.sql.Time;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 8/17/2015 10:39 AM
 */
public interface ChessAPI {

    @GET("/api/newUser")
    SignUpResponse signUp(@Query("login") String login, @Query("password") String passwordEncoded,
                          @Query("name") String name, @Query("avatar") String imageEncoded) throws TimeoutException;

    @FormUrlEncoded
    @POST("/api/login")
    SignInResponse login(@Field("login") String login, @Field("password") String passwod) throws TimeoutException;

    @GET("/api/getUser")
    GetUserResponse getUserData(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/newFriend")
    StatusResponse newFriend(@Query("my_id") Integer myId, @Query("friend_id") Integer friendId) throws TimeoutException;

    @GET("/api/acceptFriendRequest")
    StatusResponse acceptFriendRequest(@Query("my_id") Integer myId, @Query("friend_id") Integer friendId) throws TimeoutException;

    @GET("/api/declineFriendRequest")
    StatusResponse declineFriendRequest(@Query("my_id") Integer myId, @Query("friend_id") Integer friendId) throws TimeoutException;

    @GET("/api/searchForFriend")
    SearchForFriendResponse searchForFriend(@Query("id") Integer id, @Query("name") String name) throws TimeoutException;

    @GET("/api/getFriends")
    GetFriendsResponse getFriends(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/getLatestOnlineGames")
    GetLatestOnlineGamesResponse getLatestOnlineGames(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/getLatestOfflineGames")
    GetLatestOfflineGamesResponse getLatestOfflineGames(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/getLatestTournaments")
    GetLatestTournamentsResponse getLatestTournaments(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/setLookingForOpponent")
    StatusResponse setLookingForOpponent(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/searchForRandomOpponent")
    SearchForRandomOpponentResponse searchForRandomOpponent(@Query("id") Integer id,
                                                            @Query("min_points") Integer minPoints,
                                                            @Query("max_points") Integer maxPoints) throws TimeoutException;

    @GET("/api/newTournament")
    StatusResponse newTounament(@Query("name") String name,
                                @Query("max_users") Integer maxUsers,
                                @Query("min_points") Integer minPoints,
                                @Query("max_points") Integer maxPoints,
                                @Query("type") Integer type,
                                @Query("due_date") Date dueDate) throws TimeoutException;

    @GET("/api/getTournaments")
    GetTournamentsResponse getTournaments(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/assignToTournament")
    StatusResponse assignToTournament(@Query("user_id") Integer userId, @Query("tournament_id") Integer tournamentId) throws TimeoutException;

    @GET("/api/getTournamentGames")
    GetTournamentGamesResponse getTournamentGames(@Query("id") Integer id) throws TimeoutException;

    @GET("/api/addGame")
    StatusResponse addOnlineGame(@Query("id") Integer id,
                                 @Query("opponent_id") Integer opponentId,
                                 @Query("winner") Integer winner,
                                 @Query("points") Integer points,
                                 @Query("type") Integer type) throws TimeoutException;

    @GET("/api/addGame")
    StatusResponse addOfflineGame(@Query("id") Integer id,
                                  @Query("winner") Integer winner,
                                  @Query("points") Integer points,
                                  @Query("type") Integer type,
                                  @Query("difficulty") Integer difficulty) throws TimeoutException;

    @GET("/api/addGame")
    StatusResponse addTournamentGame(@Query("id") Integer id,
                                     @Query("opponent_id") Integer opponentId,
                                     @Query("winner") Integer winner,
                                     @Query("points") Integer points,
                                     @Query("tournament_id") Integer tournamentId) throws TimeoutException;
}
