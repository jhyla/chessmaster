package com.arxcreative.chessmaster.view.fragment.dashboard;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.OfflineGamesProvider;
import com.arxcreative.chessmaster.controllers.OnlineGamesProvider;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.adapters.OfflineGamesAdapter;
import com.arxcreative.chessmaster.view.adapters.OnlineGamesAdapter;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import carbon.beta.Window;
import carbon.widget.RecyclerView;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 10:58 AM
 */
@EFragment(R.layout.fragment_offline)
public class OfflineGamesFragment extends Fragment {

    @Bean
    EventBus eventBus;

    @Bean
    OfflineGamesProvider provider;

    @ViewById(R.id.offlineGames)
    RecyclerView offlineGames;

    @Bean
    OfflineGamesAdapter adapter;

    @Bean
    AppStateController appStateController;

    @AfterViews
    public void setRecyclerView(){
        offlineGames.setAdapter(adapter);
        GridLayoutManager managerTournaments = new GridLayoutManager(
                getActivity(),
                1);

        offlineGames.setLayoutManager(managerTournaments);
    }

    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
        if(provider.isFetched()){
            adapter.notifyDataSetChanged();
        } else {
            provider.fetchOnlineGames();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }

    @Subscribe
    public void onUpdate(OfflineGamesProvider.Update update){
        if(provider.isFetched()){
            adapter.notifyDataSetChanged();
        }
    }


    @Subscribe
    public void onClick(DashboardFragment.OfflineButtonClick b){
final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
        getActivity(),
        android.R.layout.select_dialog_singlechoice);
arrayAdapter.add("Very easy");
arrayAdapter.add("Easy");
arrayAdapter.add("Medium");
arrayAdapter.add("Hard");
arrayAdapter.add("Very hard");

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select difficulty:");

        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appStateController.startOfflineGame(arrayAdapter.getItem(which));
                    }
                });
        builderSingle.show();
    }
}
