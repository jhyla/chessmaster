package com.arxcreative.chessmaster.utils;

import com.squareup.otto.Bus;

import org.androidannotations.annotations.EBean;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 3:52 PM
 */
@EBean(scope = EBean.Scope.Singleton)
public class EventBus {

    @Accessors(prefix =  "m")
    @Getter
    private final static Bus mBus = new Bus();

    public void register(Object o) {
        mBus.register(o);
    }

    public void unregister(Object o) {
        mBus.unregister(o);
    }

    public void post(Object o) {
        mBus.post(o);
    }
}
