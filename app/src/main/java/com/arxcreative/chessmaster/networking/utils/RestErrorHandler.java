package com.arxcreative.chessmaster.networking.utils;

import com.arxcreative.chessmaster.BuildConfig;

import java.util.concurrent.TimeoutException;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 8/17/2015 10:48 AM
 */
public class RestErrorHandler implements ErrorHandler {



    @Override
    public Throwable handleError(RetrofitError cause) {

        if(BuildConfig.DEBUG){
            cause.printStackTrace();
        }

        return new TimeoutException();
    }

}