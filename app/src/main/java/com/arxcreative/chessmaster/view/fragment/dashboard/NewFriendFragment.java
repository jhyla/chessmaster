package com.arxcreative.chessmaster.view.fragment.dashboard;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.widget.EditText;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.FriendsProvider;
import com.arxcreative.chessmaster.controllers.NewFriendProvider;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.adapters.NewFriendAdapter;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import carbon.widget.RecyclerView;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EFragment(R.layout.fragment_new_friend)
public class NewFriendFragment extends Fragment {

    @Bean
    EventBus eventBus;

    @Bean
    NewFriendProvider provider;

    @ViewById(R.id.users)
    RecyclerView users;

    @ViewById(R.id.search_query)
    EditText searchQuery;

    @Bean
    NewFriendAdapter newFriendAdapter;

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    @Override
    public void onResume() {
        eventBus.register(this);

        if (provider.areFriendsFetched()) {
            newFriendAdapter.notifyDataSetChanged();
//        } else {
//            provider.findFriends(searchQuery.getText().toString());
        }

        super.onResume();
    }

    @AfterViews
    public void setViews(){
        users.setAdapter(newFriendAdapter);
        GridLayoutManager managerAccepted = new GridLayoutManager(getActivity(), 1);
        users.setLayoutManager(managerAccepted);
    }

    @Subscribe
    public void onUpdate(FriendsProvider.Update update){
        if (provider.areFriendsFetched()) {
            newFriendAdapter.notifyDataSetChanged();
        }
    }

    @Click(R.id.searchButton)
    public void findFriends(){
        provider.findFriends(searchQuery.getText().toString());
        setViews();
    }
}
