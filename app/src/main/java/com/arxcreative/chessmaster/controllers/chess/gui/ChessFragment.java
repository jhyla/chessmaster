package com.arxcreative.chessmaster.controllers.chess.gui;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arxcreative.chessmaster.BuildConfig;
import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.chess.general.Board;
import com.arxcreative.chessmaster.controllers.chess.general.Game;
import com.arxcreative.chessmaster.controllers.chess.general.Move;
import com.arxcreative.chessmaster.controllers.chess.general.Piece;
import com.arxcreative.chessmaster.controllers.chess.player.Player;
import com.arxcreative.chessmaster.controllers.chess.player.PlayerAI;
import com.arxcreative.chessmaster.controllers.chess.player.PlayerHuman;
import com.arxcreative.chessmaster.controllers.chess.tools.Index;
import com.arxcreative.chessmaster.internal.DataManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

@EFragment(R.layout.fragment_chessboard)
public class ChessFragment extends Fragment {
    @ViewsById({R.id.slot_A1, R.id.slot_B1, R.id.slot_C1, R.id.slot_D1, R.id.slot_E1, R.id.slot_F1, R.id.slot_G1, R.id.slot_H1, R.id.slot_A2, R.id.slot_B2, R.id.slot_C2, R.id.slot_D2, R.id.slot_E2, R.id.slot_F2, R.id.slot_G2, R.id.slot_H2, R.id.slot_A3, R.id.slot_B3, R.id.slot_C3, R.id.slot_D3, R.id.slot_E3, R.id.slot_F3, R.id.slot_G3, R.id.slot_H3, R.id.slot_A4, R.id.slot_B4, R.id.slot_C4, R.id.slot_D4, R.id.slot_E4, R.id.slot_F4, R.id.slot_G4, R.id.slot_H4, R.id.slot_A5, R.id.slot_B5, R.id.slot_C5, R.id.slot_D5, R.id.slot_E5, R.id.slot_F5, R.id.slot_G5, R.id.slot_H5, R.id.slot_A6, R.id.slot_B6, R.id.slot_C6, R.id.slot_D6, R.id.slot_E6, R.id.slot_F6, R.id.slot_G6, R.id.slot_H6, R.id.slot_A7, R.id.slot_B7, R.id.slot_C7, R.id.slot_D7, R.id.slot_E7, R.id.slot_F7, R.id.slot_G7, R.id.slot_H7, R.id.slot_A8, R.id.slot_B8, R.id.slot_C8, R.id.slot_D8, R.id.slot_E8, R.id.slot_F8, R.id.slot_G8, R.id.slot_H8})
    ArrayList<ImageView> slots;

    @ViewById
    TextView nameMy;

    @ViewById
    TextView nameHis;

    @ViewById
    TextView pointsMy;

    @ViewById
    TextView pointsHis;

    @ViewById(R.id.overAllTime)
    TextView overallTime;

    @ViewById(R.id.timeForMove)
    TextView timeForMove;

    @Bean
    AppStateController appStateController;

    @Bean
    DataManager dataManager;

    private static boolean humanVsHumanMode;
    private static boolean humanVsAiMode;
    private static boolean aiVsAiMode;

    private int concecutMEvent = 0;

    Player player;
    private Player player1, player2;
    private PlayerAI playerAI, playerAI2;

    private static Board board;
    private Game game;
    private Move move;

    private boolean srcSelected = false;
    private boolean destSelected = false;
    private String selectedSrc;
    private String selectedDest;
    private String clickedSquare;

    private boolean showLegalMoves = true;

    private final ArrayList<Point> globalPoints = new ArrayList<>();
    private final HashMap<String, Drawable> painted = new HashMap<>();

    @AfterViews
    public void initAfterViews() {
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                globalPoints.add(new Point(i,j));
            }
        }

        nameMy.setText(dataManager.getUser().getName());
        pointsMy.setText("Points: "+dataManager.getUser().getPoints());

        player = new Player();
        initGame();
        getGame().setWhosTurn(0); //white starts

        if (appStateController.isPlayingOffline()) {
            initHumanVsAiMode();
            nameHis.setText(appStateController.getGameLevelString());
            pointsHis.setText("");
        } else {
            //
        }
    }

    private boolean isHumanVsAiMode(){
        return humanVsAiMode;
    }

    /**
     * Set move object.
     * @param move
     */
    public void setMove(Move move) {
        this.move = move;
    }

    /**
     * Returns true if source square is selected, false otherwise.
     * @return
     */
    public boolean isSrcSelected() {
        return srcSelected;
    }

    /**
     * Set selected source square.
     * @param srcSelected
     */
    public void setSrcSelected(boolean srcSelected) {
        this.srcSelected = srcSelected;
    }

    /**
     * Returns true if destination square is selected, false otherwise.
     * @return
     */
    public boolean isDestSelected() {
        return destSelected;
    }

    /**
     * Set selected destination square.
     * @param destSelected
     */
    public void setDestSelected(boolean destSelected) {
        this.destSelected = destSelected;
    }

    /**
     * Get clicked square.
     * @return
     */
    public String getClickedSquare() {
        return clickedSquare;
    }

    /**
     * Set clicked square.
     * @param clickedSquare
     */
    public void setClickedSquare(String clickedSquare) {
        this.clickedSquare = clickedSquare;
    }

    /**
     * Get selected source square.
     * @return
     */
    public String getSelectedSrc() {
        return selectedSrc;
    }

    /**
     * Set selected source square.
     * @param selectedSrc
     */
    public void setSelectedSrc(String selectedSrc) {
        this.selectedSrc = selectedSrc;
    }

    /**
     * Get selected destination square.
     * @return
     */
    public String getSelectedDest() {
        return selectedDest;
    }

    /**
     * Set selected destination square.
     * @param selectedDest
     */
    public void setSelectedDest(String selectedDest) {
        this.selectedDest = selectedDest;
    }


    /**
     * Initialises game.
     */
    private void initGame() {
        board = new Board();
        move = new Move(board);
        game = new Game();
        createPlayers();
    }

    /**
     * Get game object.
     *
     * @return
     */
    private Game getGame() {
        return game;
    }

    /**
     * Creates players.
     */
    private void createPlayers() {
        //create players
        playerAI = new PlayerAI(board, getMove(), 1);
        playerAI2 = new PlayerAI(board, getMove(), 0);
        player1 = new PlayerHuman(0);
        player2 = new PlayerHuman(1);
    }

    /**
     * Get move object.
     *
     * @return
     */
    public Move getMove() {
        return move;
    }

    /**
     * Initialises human vs ai mode.
     */
    private void initHumanVsAiMode() {
        board.setPieces(new ArrayList<Piece>(32)); //set empty piece list
        board.createPieces(); //create piece objects
        createPlayers();
        aiVsAiMode = false;
        humanVsHumanMode = false;
        humanVsAiMode = true;
        repaint();
    }

    @Click({R.id.slot_A1, R.id.slot_B1, R.id.slot_C1, R.id.slot_D1, R.id.slot_E1, R.id.slot_F1, R.id.slot_G1, R.id.slot_H1, R.id.slot_A2, R.id.slot_B2, R.id.slot_C2, R.id.slot_D2, R.id.slot_E2, R.id.slot_F2, R.id.slot_G2, R.id.slot_H2, R.id.slot_A3, R.id.slot_B3, R.id.slot_C3, R.id.slot_D3, R.id.slot_E3, R.id.slot_F3, R.id.slot_G3, R.id.slot_H3, R.id.slot_A4, R.id.slot_B4, R.id.slot_C4, R.id.slot_D4, R.id.slot_E4, R.id.slot_F4, R.id.slot_G4, R.id.slot_H4, R.id.slot_A5, R.id.slot_B5, R.id.slot_C5, R.id.slot_D5, R.id.slot_E5, R.id.slot_F5, R.id.slot_G5, R.id.slot_H5, R.id.slot_A6, R.id.slot_B6, R.id.slot_C6, R.id.slot_D6, R.id.slot_E6, R.id.slot_F6, R.id.slot_G6, R.id.slot_H6, R.id.slot_A7, R.id.slot_B7, R.id.slot_C7, R.id.slot_D7, R.id.slot_E7, R.id.slot_F7, R.id.slot_G7, R.id.slot_H7, R.id.slot_A8, R.id.slot_B8, R.id.slot_C8, R.id.slot_D8, R.id.slot_E8, R.id.slot_F8, R.id.slot_G8, R.id.slot_H8})
    public void onClicked(View view) {
        //identify which player turn it is
        int color = getGame().getWhosTurn();
        if (color == 0) {
            player = player1;
        } else {
            player = player2;
        }
        //set ready to accept mouse click for destination square
        if (concecutMEvent == 1) {
            concecutMEvent = 2;
        }

        //set the clicked square by mapping coordinates to notation
        setClickedSquare(slotToNotation(view));

        //if source square hasn't been selected
        if (!isSrcSelected()) {
            //check if clicked source square has player's own piece
            if (isSrcSqValid(getClickedSquare(), getMove(), board, player)) {
                //set source square as selected
                setSrcSelected(true);
                //set clicked square as selected source
                setSelectedSrc(getClickedSquare());
                concecutMEvent = 1;
                if (showLegalMoves) {
//                    TODO notify data set changed
                    log("ON CLICK !isSourceSelected showLegalMoves");
                    repaint();
                }
            } else {
                //TODO TOast not valid
//                JOptionPane.showMessageDialog(f, "Source square not valid.",
//                        "Move error", JOptionPane.PLAIN_MESSAGE);
            }
        }
        if (concecutMEvent == 2) {
            //if source is selected but not destination
            if (isSrcSelected() && !isDestSelected()) {
                //check if clicked destination square is empty or has enemy piece
                if(getClickedSquare().equals(getSelectedSrc())){
                    setSrcSelected(false);
                    removeHighlights();
                    repaint();
                    concecutMEvent = 0;
                    return;
                }
                if (isDestSqValid(getSelectedSrc(), getClickedSquare(), getMove(),
                        board, player)) {
                    //set destination square as selected
                    setDestSelected(true);
                    //set clicked square as selected destination
                    setSelectedDest(getClickedSquare());
                    //reset concecutive mouse event
                    concecutMEvent = 0;
                } else {
                    // TODO toast
//                    JOptionPane.showMessageDialog(f, "Destination square not valid.",
//                            "Move error", JOptionPane.PLAIN_MESSAGE);
                }
            }
        }
        //if valid source and destination is selected
        if (isSrcSelected() && isDestSelected()) {
            //do move
            log("getSelectedDestination()" + getSelectedDest());
            movePiece(board, getMove(), player, getSelectedSrc(), getSelectedDest());
            //repaint the board
            setSrcSelected(false);
            setDestSelected(false);
            removeHighlights();
            log("onClick isSrcSelected && isDestSelected");
            repaint();
            // TODO repaint
            //set source and destination unselected after
            //the movement has been done
            //check if check mate
            if (playerAI.isCheckmate(player1)) {
                //TODO
//                        JOptionPane.showMessageDialog(f, "Check mate",
//                                "Chess", JOptionPane.PLAIN_MESSAGE);
            }
            //another player's turn
//                    if (isHumanVsHumanMode()) {
//                        if (getGame().getWhosTurn()==0) {
//                            getGame().setWhosTurn(1);
//                        } else {
//                            getGame().setWhosTurn(0);
//                        }
//                    }
            if (isHumanVsAiMode()) {
                playerAI.doBestMove(playerAI);
                log("isHumanVsAIMove");
                repaint();
                // TODO repaint
            }
        }
    }

    private void removeHighlights() {
        for(final String s : painted.keySet()){
            log("Clearing: " + s);
            final View v = notationToSlot(s);
            v.post(new Runnable() {
                @Override
                public void run() {
                    v.setBackground(painted.get(s));
                }
            });
        }
        painted.clear();
    }

    /**
     * Checks if the selected destination square is valid.
     * @param srcSq
     * @param destSq
     * @param move
     * @param board
     * @param player
     * @return
     */
    // TODO
    public boolean isDestSqValid(String srcSq, String destSq, Move move, Board board, Player player) {
        //check source square notation for validity
        if (!move.checkSqValidity(destSq)) {
            return false;
        }
        Piece piece = board.notationToPiece(srcSq);
        //get all movements that are allowed for the selected piece
        ArrayList<ArrayList<Integer>> legalMoves = move.possiblePieceMoves(piece, false);
        //array coordinates for new destination
        Index newLoc = board.notationToIndex(destSq);
        //find out if destination location is included in the legal moves list
        ArrayList<Integer> x = legalMoves.get(0); //list of row numbers
        ArrayList<Integer> y = legalMoves.get(1); //list of column numbers
        ListIterator<Integer> xList = x.listIterator();  //row iterator
        ListIterator<Integer> yList = y.listIterator();  //column iterator
        int xL, yL;
        while (xList.hasNext() && yList.hasNext()) { //while lists have coordinates
            //listiterator next() method doesn't work inside if statement -> assign to variables
            xL = xList.next();
            yL = yList.next();
            if (newLoc.getX()==xL && newLoc.getY()==yL) { //legal move
                return true;
            }
        }
        return false;
    }

    /**
     * Moves piece.
     * @param board
     * @param move
     * @param player
     * @param src
     * @param dest
     */
    public void movePiece(Board board, Move move, Player player, String src, String dest) {
        //get piece to move
        Piece p = board.notationToPiece(src);
        //array coordinates for new destination
        Index newLoc = board.notationToIndex(dest);
        //remove captured piece from the board
        board.removePiece(newLoc.getX(), newLoc.getY());
        p.setRow(newLoc.getX()); //change piece row
        p.setCol(newLoc.getY()); //change piece column
        //place source and destination square to history of moves
        if (player.getSide()==0) { //if white
            //add white piece move to history
            move.getHistoryOfMoves().addWhiteMove(src, dest);
        } else if (player.getSide()==1) { //if black
            //add black piece move to history
            move.getHistoryOfMoves().addBlackMove(src, dest);
        }
        //promote pawns to queens if they reach enemy's end
        board.promotePawnsToQueen(player.getSide());
    }

    private String slotToNotation(View view) {
        return view.getTag().toString();
    }

    /**
     * Checks if the selected source square is valid.
     * @param srcSq
     * @param move
     * @param board
     * @param player
     * @return
     */
    public boolean isSrcSqValid(String srcSq, Move move, Board board, Player player) {
        //check source square notation for validity
        if (!move.checkSqValidity(srcSq)) {
            return false;
        }
        //check if own piece exists on selected source square.
        ArrayList<Piece> ownPieces = board.getPiecesFromOneSide(player.getSide());
        for (Piece p : ownPieces) {
            if (board.coordinatesToNotation(p.getRow(), p.getCol()).equalsIgnoreCase(srcSq)) {
                return true;
            }
        }
        return false;
    }

    private void repaint(){
        int blue = Color.rgb(75, 50, 50);
        int orange = Color.rgb(246, 213, 83);
        if (isSrcSelected()) {


            if (showLegalMoves) {
                //paint own piece's background
                paintSquare(getSelectedSrc(), blue);
                //paint legal moves background
//                log("Selected source: "+getSelectedSrc() + " Selected destination:" + getSelectedDest());
//                Piece piece = board.notationToPiece(getSelectedSrc());
//                //get all movements that are allowed for the selected piece
//                ArrayList<ArrayList<Integer>> legalMoves = move.possiblePieceMoves(piece, false);
//                ArrayList<Integer> x = legalMoves.get(0); //list of row numbers
//                ArrayList<Integer> y = legalMoves.get(1); //list of column numbers
//                ListIterator<Integer> xList = x.listIterator();  //row iterator
//                ListIterator<Integer> yList = y.listIterator();  //column iterator
//                while (xList.hasNext() && yList.hasNext()) { //while lists have coordinates
//                    //paint squares where the selected piece is allowed to move
//                    paintSquare(board.coordinatesToNotation(xList.next(), yList.next()), orange);
//                }
            }
        }
        drawPieces();
    }

    private void paintSquare(String selectedSrc, int blue) {
        View v = notationToSlot(selectedSrc);
        if(v!=null){
            painted.put(selectedSrc, ((ImageView) v).getBackground());
            v.setBackgroundColor(blue);
        }
    }

    private View notationToSlot(String selectedSrc) {
//        log(selectedSrc);
        return this.getView().findViewWithTag(selectedSrc);
    }

    private void log(String s) {
        if(BuildConfig.DEBUG){
            Log.i("--CHESS--", s);
        }
    }

    /**
     * Draws pieces.
     */
    public void drawPieces() {
        //get all pieces
        ArrayList<Piece> pieces = board.getPieces();
        //draw all pieces
        ArrayList<Point> allPoints = new ArrayList<>();
        allPoints.addAll(globalPoints);

        int resource = -1;
        for (Piece p : pieces) {
            allPoints.remove(new Point(p.getCol(),p.getRow()));
            if (p.getColor()==0) { //white piece
                //get image
                switch(p.getType()) {
                    case 1:
                        resource = R.drawable.wk;
                        break;
                    case 2:
                        resource = R.drawable.wq;
                        break;
                    case 3:
                        resource = R.drawable.wr;
                        break;
                    case 4:
                        resource = R.drawable.wn;
                        break;
                    case 5:
                        resource = R.drawable.wb;
                        break;
                    case 6:
                        resource = R.drawable.wp;
                        break;
                    default:
                        break;
                }
            } else if (p.getColor()==1) { //black piece
                //get image
                switch(p.getType()) {
                    case 1:
                        resource = R.drawable.bk;
                        break;
                    case 2:
                        resource = R.drawable.bq;
                        break;
                    case 3:
                        resource = R.drawable.br;
                        break;
                    case 4:
                        resource = R.drawable.bn;
                        break;
                    case 5:
                        resource = R.drawable.bb;
                        break;
                    case 6:
                        resource = R.drawable.bp;
                        break;
                    default:
                        break;
                }
            }
            if (p!=null) {
                String notation = board.coordinatesToNotation(p.getRow(), p.getCol());
                ImageView v = (ImageView) notationToSlot(notation);
                v.setImageResource(resource);
            } else {
                String notation = board.coordinatesToNotation(p.getRow(), p.getCol());
                ImageView v = (ImageView) notationToSlot(notation);
                v.setImageResource(R.drawable.transparent);
            }
        }
        for(Point p: allPoints){
            String notation = board.coordinatesToNotation(p.y, p.x);
            ImageView v = (ImageView) notationToSlot(notation);
            v.setImageResource(R.drawable.transparent);
        }
    }
}
