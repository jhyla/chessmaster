package com.arxcreative.chessmaster.view.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.arxcreative.chessmaster.controllers.NewFriendProvider;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.view.component.NewFriendView;
import com.arxcreative.chessmaster.view.component.NewFriendView_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Belhaver on 2015-12-01.
 */
@EBean
public class NewFriendAdapter extends RecyclerViewAdapterBase<UserModel, NewFriendView>{

    @Bean
    NewFriendProvider provider;

    @RootContext
    Context context;

    @Override
    protected NewFriendView onCreateItemView(ViewGroup parent, int viewType){
        return NewFriendView_.build(context);
    }

    @Override
    public UserModel getObject(int position) { return provider.getFriend(position); }

    @Override
    public void onBindViewHolder(ViewWrapper<NewFriendView> holder, int position) {
        holder.getView().bind(getObject(position));
    }

    @Override
    public int getItemCount() {
        return provider.getNewFriendsCount();
    }
}
