package com.arxcreative.chessmaster.view.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.arxcreative.chessmaster.controllers.TournamentsProvider;
import com.arxcreative.chessmaster.model.TournamentModel;
import com.arxcreative.chessmaster.view.component.LatestTournamentView;
import com.arxcreative.chessmaster.view.component.LatestTournamentView_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 11:34 AM
 */
@EBean
public class LatestTournamentsAdapter extends RecyclerViewAdapterBase<TournamentModel, LatestTournamentView> {

    @Bean
    TournamentsProvider provider;

    @RootContext
    Context context;

    @Override
    protected LatestTournamentView onCreateItemView(ViewGroup parent, int viewType) {
        return LatestTournamentView_.build(context);
    }

    @Override
    public TournamentModel getObject(int position) {
        return provider.getLatestTournament(position);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<LatestTournamentView> holder, int position) {
        holder.getView().bind(getObject(position));
    }

    @Override
    public int getItemCount() {
        return provider.getLatestTournamentsCount();
    }

}
