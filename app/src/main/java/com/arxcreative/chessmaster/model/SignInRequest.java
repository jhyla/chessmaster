package com.arxcreative.chessmaster.model;

import lombok.Setter;
import lombok.experimental.Builder;

/**
 * @aring uthor Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 12:06 PM
 */
@Setter
@Builder
public class SignInRequest {
    String login;
    String password;
}
