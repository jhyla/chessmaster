package com.arxcreative.chessmaster.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/22/2015 9:13 PM
 */
@Getter
@Setter
public class SignUpResponse {
    Boolean status;
    UserModel user;
    String error;
}
