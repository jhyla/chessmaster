package com.arxcreative.chessmaster.controllers;

import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.GetLatestTournamentsResponse;
import com.arxcreative.chessmaster.model.GetTournamentsResponse;
import com.arxcreative.chessmaster.model.TournamentModel;
import com.arxcreative.chessmaster.networking.RestClient;
import com.arxcreative.chessmaster.utils.EventBus;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 11:57 AM
 */
@EBean(scope = EBean.Scope.Singleton)
public class TournamentsProvider {

    public enum State{
        Fetched, Fetching, Error
    }

    @Bean
    DataManager dataManager;

    @Bean
    EventBus eventBus;

    private State latestTournamentsState = null;
    private State tournamentsState = null;

    private final ArrayList<TournamentModel> tournaments = new ArrayList<>();
    private final ArrayList<TournamentModel> latestTournaments = new ArrayList<>();

    private String latestTournamentsError;
    private String tournamentsError;

    @AfterInject
    public void registerBus(){
        eventBus.register(this);
    }

    @Background
    public void fetchTournaments() {
        GetTournamentsResponse response = null;
        try{
            response = RestClient.getInstance().getTournaments(dataManager.getUser().getId());
        } catch(TimeoutException exception){
            onFetchTournamentsUnknownError();
            return;
        }

        if(response.getStatus()){
            onFetchTournamentSuccess(response.getTournaments());
        } else {
            onFetchTournamentFailure(response.getError());
        }
    }

    @Background
    public void fetchLatestTournaments() {
        GetLatestTournamentsResponse response = null;
        try{
            response = RestClient.getInstance().getLatestTournaments(dataManager.getUser().getId());
        } catch(TimeoutException exception){
            onFetchLatestTournamentsUnknownError();
            return;
        }

        if(response.getStatus()){
            onFetchLatestTournamentSuccess(response.getTournaments());
        } else {
            onFetchLatestTournamentFailure(response.getError());
        }
    }

    @UiThread
    public void onFetchLatestTournamentFailure(String error) {
        this.latestTournamentsError = error;
        setFetchLatestTournamentState(State.Error);
    }

    @UiThread
    public void onFetchLatestTournamentSuccess(ArrayList<TournamentModel> tournaments) {
        this.latestTournaments.clear();
        this.latestTournaments.addAll(tournaments);
        setFetchLatestTournamentState(State.Fetched);
    }

    @UiThread
    public void onFetchLatestTournamentsUnknownError() {
        this.latestTournamentsError = "Unknown error";
        setFetchLatestTournamentState(State.Error);
    }

    @UiThread
    public void onFetchTournamentsUnknownError() {
        this.tournamentsError = "Unknown error";
        setFetchTournamentState(State.Error);
    }

    @UiThread
    public void onFetchTournamentFailure(String error) {
        this.tournamentsError = error;
        setFetchTournamentState(State.Error);
    }

    @UiThread
    public void onFetchTournamentSuccess(ArrayList<TournamentModel> tournaments) {
        if(tournaments==null)
            tournaments=new ArrayList<>();
        this.tournaments.clear();
        this.tournaments.addAll(tournaments);
        setFetchTournamentState(State.Fetched);
    }

    private void setFetchTournamentState(State state){
        this.tournamentsState = state;
        eventBus.post(new Update());
    }

    private void setFetchLatestTournamentState(State state){
        this.latestTournamentsState = state;
        eventBus.post(new Update());
    }

    public boolean areLatestTournamentsFetched(){
        return latestTournamentsState==State.Fetched;
    }

    public boolean areTournamentsFetched(){
        return tournamentsState == State.Fetched;
    }

    public boolean hasLatestTournamentsError(){
        return latestTournamentsState == State.Error;
    }

    public boolean hasTournamentsError(){
        return tournamentsState == State.Error;
    }

    public String getLatestTournamentsError(){
        return latestTournamentsError;
    }

    public String getTournamentsError(){
        return tournamentsError;
    }

    public int getTournamentsCount() {
        return tournaments.size();
    }

    public int getLatestTournamentsCount(){
        return latestTournaments.size();
    }

    public TournamentModel getTournament(int position) {
        return tournaments.get(position);
    }

    public TournamentModel getLatestTournament(int position) {
        return latestTournaments.get(position);
    }

    public class Update{

    }
}
