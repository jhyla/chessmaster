package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.model.UserModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;


/**
 * Created by Belhaver on 2015-12-01.
 */
@EViewGroup(R.layout.item_friend)
public class AcceptedFriendsView extends LinearLayout {

    @ViewById(R.id.avatar)
    ImageView avatar;

    @ViewById(R.id.name)
    TextView name;

    public AcceptedFriendsView(Context context) { super(context); }

    public AcceptedFriendsView(Context context, AttributeSet attrs)  {
        super(context, attrs);
    }

    public AcceptedFriendsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(UserModel model){
        name.setText(model.getName());
        ImageLoader.getInstance().displayImage("http://" + model.getAvatar(), avatar);
    }
}
