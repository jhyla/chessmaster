package com.arxcreative.chessmaster.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/22/2015 9:13 PM
 */
@Getter
@Setter
public class UserModel {
    Integer id;
    String login;
    String name;
    String avatar;
    Date dateCreated;
    Integer points;
}
