package com.arxcreative.chessmaster.view.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.chess.gui.ChessFragment;
import com.arxcreative.chessmaster.controllers.chess.gui.ChessFragment_;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment_;
import com.arxcreative.chessmaster.view.fragment.LoginFragment;
import com.arxcreative.chessmaster.view.fragment.LoginFragment_;
import com.arxcreative.chessmaster.view.fragment.RegisterFragment;
import com.arxcreative.chessmaster.view.fragment.RegisterFragment_;
import com.arxcreative.chessmaster.view.fragment.dashboard.NewFriendFragment;
import com.arxcreative.chessmaster.view.fragment.dashboard.NewFriendFragment_;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/21/2015 3:53 PM
 */

@WindowFeature({Window.FEATURE_NO_TITLE})

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity{

    private static final String CONTAINTER_TAG = "FRAGMENTS";
    @Bean
    EventBus mBus;

    @Bean
    AppStateController appStateController;

    @Override
    protected void onPause() {
        mBus.unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        mBus.register(this);
        super.onResume();
        updateFragments();
    }

    private void updateFragments() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(CONTAINTER_TAG);
        Fragment newFragment;

        boolean drawerLocked = false;
        switch (appStateController.getCurrentState()) {

            case REGISTER:
                if (currentFragment instanceof RegisterFragment)
                    return;
                newFragment = new RegisterFragment_();
                break;

            case LOGIN:
                if (currentFragment instanceof LoginFragment)
                    return;
                newFragment = new LoginFragment_();
                break;

            case DASHBOARD:
                if(currentFragment instanceof DashboardFragment)
                    return;
                newFragment = new DashboardFragment_();
                break;

            case OFFLINE:
                if(currentFragment instanceof ChessFragment)
                    return;;
                newFragment = new ChessFragment_();
                break;

            case NEW_FRIEND:
                if (currentFragment instanceof NewFriendFragment)
                    return;
                newFragment = new NewFriendFragment_();
                break;
            default:
                throw new IllegalArgumentException("No fragment to display!");
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, newFragment, CONTAINTER_TAG)
                .commit();
    }

    @Subscribe
    public void displayCurrentFragment(AppStateController.Update event) {
        updateFragments();
    }

    @Override
    public void onBackPressed(){
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(CONTAINTER_TAG);
        Fragment newFragment;

        if (currentFragment instanceof NewFriendFragment){
            newFragment = new DashboardFragment_();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, newFragment, CONTAINTER_TAG)
                    .commit();
        } else {
            finish();
        }
    }
}
