package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.model.GameModel;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import carbon.beta.Window;
import carbon.widget.LinearLayout;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 4:08 PM
 */
@EViewGroup(R.layout.item_offline_recent)
public class OfflineGameView extends LinearLayout{

    @ViewById(R.id.playerOneName)
    TextView level;
    @ViewById(R.id.winner)
    TextView winner;
    @ViewById(R.id.points)
    TextView points;

    public OfflineGameView(Context context) {
        super(context);
    }

    public OfflineGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OfflineGameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(GameModel model){

        String levelName = null;
        switch (model.getDifficulty()){
            case 0:
                levelName = "Level: Very Easy";
                break;
            case 1:
                levelName = "Level: Easy";
                break;
            case 2:
                levelName = "Level: Medium";
                break;
            case 3:
                levelName = "Level: Hard";
                break;
            case 4:
                levelName = "Level: Very Hard";
                break;
            case 5:
                levelName = "Level: Impossible";
                break;
        }
        level.setText(levelName);
        points.setText("Points: "+model.getPoints()+"");
        winner.setText(model.getWinner() == -1 ? "Game lost" : "Game won");
    }
}
