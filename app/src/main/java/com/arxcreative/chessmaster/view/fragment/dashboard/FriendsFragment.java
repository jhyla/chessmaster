package com.arxcreative.chessmaster.view.fragment.dashboard;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.AppStateController;
import com.arxcreative.chessmaster.controllers.FriendsProvider;
import com.arxcreative.chessmaster.model.UserModel;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.adapters.AcceptedFriendsAdapter;
import com.arxcreative.chessmaster.view.adapters.PendingFriendsAdapter;
import com.arxcreative.chessmaster.view.adapters.SentFriendsAdapter;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import carbon.widget.RecyclerView;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 10:58 AM
 */
@EFragment(R.layout.fragment_friends)
public class FriendsFragment extends Fragment {

    @Bean
    EventBus eventBus;

    @Bean
    FriendsProvider provider;

    @Bean
    AppStateController appStateController;

    @ViewById(R.id.acceptedFriends)
    RecyclerView acceptedFriends;

    @ViewById(R.id.pendingFriends)
    RecyclerView pendingFriends;

    @ViewById(R.id.sentFriends)
    RecyclerView sentFriends;

    @ViewById(R.id.acceptButton)
    TextView acceptButton;

    @ViewById(R.id.rejectButton)
    TextView rejectButton;

    @Bean
    AcceptedFriendsAdapter acceptedAdapter;

    @Bean
    PendingFriendsAdapter pendingAdapter;

    @Bean
    SentFriendsAdapter sentAdapter;

    @Override
    public void onPause() {
        eventBus.unregister(this);
        super.onPause();
    }

    @Override
    public void onResume() {
        eventBus.register(this);

        if (provider.areFriendsFetched()) {
            acceptedAdapter.notifyDataSetChanged();
            pendingAdapter.notifyDataSetChanged();
            sentAdapter.notifyDataSetChanged();
        } else {
            provider.fetchFriends();
        }

        super.onResume();
    }

    @AfterViews
    public void setViews(){
        acceptedFriends.setAdapter(acceptedAdapter);
        GridLayoutManager managerAccepted = new GridLayoutManager(getActivity(), 1);
        acceptedFriends.setLayoutManager(managerAccepted);


        pendingFriends.setAdapter(pendingAdapter);
        GridLayoutManager managerPending = new GridLayoutManager(getActivity(), 1);
        pendingFriends.setLayoutManager(managerPending);

        sentFriends.setAdapter(sentAdapter);
        GridLayoutManager managerSent = new GridLayoutManager(getActivity(), 1);
        sentFriends.setLayoutManager(managerSent);
    }

    @Subscribe
    public void onUpdate(FriendsProvider.Update update){
        if (provider.areFriendsFetched()) {
            acceptedAdapter.notifyDataSetChanged();
            pendingAdapter.notifyDataSetChanged();
            sentAdapter.notifyDataSetChanged();
        }
    }
    
    @Subscribe
    public void goToNewFriendFragment(DashboardFragment.FriendsButtonClick b){
        appStateController.openNewFriend();
    }
}
