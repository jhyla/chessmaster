package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.internal.DataManager;
import com.arxcreative.chessmaster.model.TournamentModel;
import com.arxcreative.chessmaster.model.UserModel;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import carbon.widget.LinearLayout;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 1:17 PM
 */
@EViewGroup(R.layout.item_tournament_recent)
public class LatestTournamentView extends LinearLayout{

    @Bean
    DataManager dataManager;

    @ViewById(R.id.players)
    TextView players;

    @ViewById(R.id.yourPosition)
    TextView yourPosition;

    @ViewById(R.id.name)
    TextView name;

    public LatestTournamentView(Context context) {
        super(context);
    }

    public LatestTournamentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatestTournamentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(TournamentModel model){
        name.setText(model.getName());
        players.setText("Players: "+model.getPlayers().size());

        String yourPositionText = null;
        int i=1;
        for(UserModel u:model.getPlayers()){
            if(u.getId()==dataManager.getUser().getId()){
                yourPositionText = i+"";
            }
        }

        yourPosition.setText("Your position: "+yourPositionText);

    }
}
