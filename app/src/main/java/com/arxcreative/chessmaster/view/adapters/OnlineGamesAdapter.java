package com.arxcreative.chessmaster.view.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.arxcreative.chessmaster.controllers.OnlineGamesProvider;
import com.arxcreative.chessmaster.model.GameModel;
import com.arxcreative.chessmaster.view.component.OnlineGamesView;
import com.arxcreative.chessmaster.view.component.OnlineGamesView_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 1:46 PM
 */
@EBean
public class OnlineGamesAdapter extends RecyclerViewAdapterBase<GameModel, OnlineGamesView>{

    @Bean
    OnlineGamesProvider provider;

    @RootContext
    Context context;

    @Override
    protected OnlineGamesView onCreateItemView(ViewGroup parent, int viewType) {
        return OnlineGamesView_.build(context);
    }

    @Override
    public GameModel getObject(int position) {
        return provider.getGame(position);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<OnlineGamesView> holder, int position) {
        holder.getView().bind(getObject(position));
    }

    @Override
    public int getItemCount() {
        return provider.getCount();
    }
}
