package com.arxcreative.chessmaster.view.fragment.dashboard;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.widget.ArrayAdapter;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.controllers.OnlineGamesProvider;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.adapters.OnlineGamesAdapter;
import com.arxcreative.chessmaster.view.fragment.DashboardFragment;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import carbon.widget.RecyclerView;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 10:58 AM
 */
@EFragment(R.layout.fragment_online)
public class OnlineGamesFragment extends Fragment {

    @Bean
    EventBus eventBus;

    @Bean
    OnlineGamesProvider provider;

    @ViewById(R.id.onlineGames)
    RecyclerView onlineGames;

    @Bean
    OnlineGamesAdapter adapter;

    @AfterViews
    public void setRecyclerView(){
        onlineGames.setAdapter(adapter);
        GridLayoutManager managerTournaments = new GridLayoutManager(
                getActivity(),
                1);

        onlineGames.setLayoutManager(managerTournaments);
    }

    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
        if(provider.isFetched()){
            adapter.notifyDataSetChanged();
        } else {
            provider.fetchOnlineGames();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }

    @Subscribe
    public void onUpdate(OnlineGamesProvider.Update update){
        if(provider.isFetched()){
            adapter.notifyDataSetChanged();
        }
    }

    @Subscribe
    public void onClick(DashboardFragment.OnlineButtonClick b){
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Random opponent");
        arrayAdapter.add("Friend");

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select one:");

        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        switch (which) {
                            case 1:
                                Snackbar.make(OnlineGamesFragment.this.getView(), "None of your friends is online", Snackbar.LENGTH_LONG).show();
                                break;
                            case 0:
                                Snackbar.make(OnlineGamesFragment.this.getView(), "Please wait", Snackbar.LENGTH_LONG).show();
                                showNoRandom();
                                break;
                        }
                    }
                });
        builderSingle.show();
    }

    @Background(delay = 5000)
    public void showNoRandom() {
        showNoRandomOnUI();
    }

    @UiThread
    public void showNoRandomOnUI() {
        Snackbar.make(OnlineGamesFragment.this.getView(), "No online players currently, try later.", Snackbar.LENGTH_LONG).show();
    }
}
