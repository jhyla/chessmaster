package com.arxcreative.chessmaster.networking;

import com.arxcreative.chessmaster.networking.utils.RestErrorHandler;
import com.arxcreative.chessmaster.utils.DateUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/22/2015 8:55 PM
 */

public class RestClient {
    public static String REST_URL = "http:/arx-creative.com/chess/";
    private static RestClient sRestClient;
    private ChessAPI mDefaultApiService;
    private Gson mGson;

    public static ChessAPI getInstance() {
        if (sRestClient == null) {
            sRestClient = new RestClient();
        }
        return sRestClient.mDefaultApiService;
    }

    public RestClient() {
        mGson = new GsonBuilder()
                .setDateFormat(DateUtils.SERVER_DATE_FORMAT)                                           //2015-12-26 09:00
                .create();

        RestAdapter.Builder builder = createRestAdapterBuilder();
        mDefaultApiService = builder.build().create(ChessAPI.class);
    }


    private RestAdapter.Builder createRestAdapterBuilder() {
        OkHttpClient mOkHttpClient = new OkHttpClient();
        mOkHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);
        mOkHttpClient.setReadTimeout(120, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(REST_URL)
                .setClient(new OkClient(mOkHttpClient))
                .setErrorHandler(new RestErrorHandler())
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                    }
                })
                .setConverter(new GsonConverter(mGson));
    }
}