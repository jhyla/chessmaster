package com.arxcreative.chessmaster.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 4:16 PM
 */
@Getter
@Setter
public class RankingModel {
    Integer player_id;
    Integer points;
}
