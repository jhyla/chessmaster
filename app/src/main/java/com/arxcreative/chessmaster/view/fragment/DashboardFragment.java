package com.arxcreative.chessmaster.view.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.utils.EventBus;
import com.arxcreative.chessmaster.view.fragment.dashboard.FriendsFragment;
import com.arxcreative.chessmaster.view.fragment.dashboard.FriendsFragment_;
import com.arxcreative.chessmaster.view.fragment.dashboard.NewFriendFragment_;
import com.arxcreative.chessmaster.view.fragment.dashboard.OfflineGamesFragment;
import com.arxcreative.chessmaster.view.fragment.dashboard.OfflineGamesFragment_;
import com.arxcreative.chessmaster.view.fragment.dashboard.OnlineGamesFragment;
import com.arxcreative.chessmaster.view.fragment.dashboard.OnlineGamesFragment_;
import com.arxcreative.chessmaster.view.fragment.dashboard.TournamentGamesFragment;
import com.arxcreative.chessmaster.view.fragment.dashboard.TournamentGamesFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 10:55 AM
 */
@EFragment(R.layout.fragment_con_dashboard)
public class DashboardFragment extends Fragment{

    @ViewById(R.id.fragmentContainer)
    ViewPager viewPager;

    @Bean
    EventBus eventBus;

    @AfterViews
    public void setViewPager(){
        viewPager.setAdapter(new CustomPagerAdapter(getChildFragmentManager(),getActivity()));
    }

    class CustomPagerAdapter extends FragmentPagerAdapter {

        Context mContext;

        public CustomPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int position) {

            // Create fragment object
            Fragment fragment = new Fragment();

            switch(position){
                case 0:
                    fragment = new OnlineGamesFragment_();
                    break;
                case 1:
                    fragment = new TournamentGamesFragment_();
                    break;
                case 2:
                    fragment = new OfflineGamesFragment_();
                    break;
                case 3:
                    fragment = new FriendsFragment_();
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case 0:
                    return "Online games";
                case 1:
                    return "Tournaments";
                case 2:
                    return "Offline games";
                case 3:
                    return "Friends";
                default:
                    return "Something's wrong";
            }
        }
    }

    @Click(R.id.button)
    public void onButtonClick(){
        switch (viewPager.getCurrentItem()){
            case 0:
                eventBus.post(new OnlineButtonClick());
                break;
            case 1:
                eventBus.post(new TournamentButtonClick());
                break;
            case 2:
                eventBus.post(new OfflineButtonClick());
                break;
            case 3:
                eventBus.post(new FriendsButtonClick());
                break;
        }
    }

    public static class OfflineButtonClick {
    }
    public static class OnlineButtonClick {
    }
    public static class TournamentButtonClick {
    }
    public static class FriendsButtonClick {
    }
}
