package com.arxcreative.chessmaster.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.arxcreative.chessmaster.R;
import com.arxcreative.chessmaster.model.TournamentModel;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

import carbon.widget.LinearLayout;

/**
 * @author Jakub Hyła jakub.hyla@ready4s.pl
 * @author Ready4s
 * @since 11/29/2015 1:17 PM
 */
@EViewGroup(R.layout.item_tournament_possible)
public class TournamentView extends LinearLayout{

    @ViewById(R.id.players)
    TextView players;

    @ViewById(R.id.maxPlayers)
    TextView maxPlayers;

    @ViewById(R.id.name)
    TextView name;

    public TournamentView(Context context) {
        super(context);
    }

    public TournamentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TournamentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(TournamentModel model){
        maxPlayers.setText("Max players: "+model.getMax_users());
        players.setText("Players: "+model.getPlayers().size());
        name.setText(model.getName());
    }
}
